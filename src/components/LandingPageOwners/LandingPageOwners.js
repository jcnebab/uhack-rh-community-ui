import React from 'react'
import Button from '../common/Button'
import Navigation from '../common/Navigation'
import Footer from '../common/Footer'

const LandingPageOwners = ({ history }) => {
	return (
		<>
			<Navigation />
			<div className="LandingPageOwners">
				<Button 
					name="Find a Rural Community to Help Today!"
					setFunction={() => history.push("/owners/communities")}
					className="ButtonInput LandingPageButton"
				/>
				<section>
					<h2>What is VARIEGoods?</h2>
					<p>VARIEGoods is created to connect consumers to business owners to rural communities who mostly rely on their agricultural products as their source of income. And we challenge business owners to bond with these communities through a partnership that will not only help them survive but also to give the SME's better and affordable products to sell to the consumers. A <i>variety of goodness</i> to promote comfortable living among Filipinos.</p>
				</section>
			</div>
			<Footer />
		</>
	)
}

export default LandingPageOwners