import React from 'react'
import Navigation from '../common/Navigation'
import Footer from '../common/Footer'
import { store } from '../util/store'

const LandingPageConsumer = props => {
    return (
        <>
            <Navigation />
            <div className="LandingPageConsumer">
                <div>
                    <h2>Best Selling</h2>
                    <div className="ProductList" >
                        {
                            store.best_selling.products.map((item, i) => (
                                <div key={`${item}-${i}`} className="product">
                                    <img src={item.url} alt={item.name} />
                                    <p>Name: {item.name}</p>
                                    <p>Price: {item.price}</p>
                                    <p>Seller: {item.seller} </p>
                                    <p>Supplier: {item.supplier}</p>
                                    {/* <button className="BuyButton hidden">Buy</button> */}
                                </div>
                            ))
                        }
                    </div>
                </div>
                <div>
                    <h2>Featured Products</h2>
                    <div className="ProductList" >
                        {
                            store.best_selling.products.map((item, i) => (
                                <div key={`${item}-${i}`} className="product">
                                    <img src={item.url} alt={item.name} />
                                    <p>Name: {item.name}</p>
                                    <p>Price: {item.price}</p>
                                    <p>Seller: {item.seller} </p>
                                    <p>Supplier: {item.supplier}</p>
                                    {/* <button className="BuyButton">Buy</button> */}
                                </div>
                            ))
                        }
                    </div>
                </div>
                <div>
                    <h2>Freshly Added Products</h2>
                    <div className="ProductList" >
                    {
                        store.best_selling.products.map((item, i) => (
                            <div key={`${item}-${i}`} className="product">
                                <img src={item.url} alt={item.name} />
                                <p>Name: {item.name}</p>
                                <p>Price: {item.price}</p>
                                <p>Seller: {item.seller} </p>
                                <p>Supplier: {item.supplier}</p>
                                {/* <button className="BuyButton">Buy</button> */}
                            </div>
                        ))
                    }
                    </div>
                </div>
            </div>
            <Footer />
        </>
    )
}

export default LandingPageConsumer
