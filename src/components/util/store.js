const imageURL = `https://storage.googleapis.com/web-media-upload/category_placeholder-paper-paper-products.png`

export const store = {
   best_selling: {
        products: [
            {
                name: `Vegetable 1`,
                price: 15.00,
                url: imageURL,
                seller: `Baguio Fresh Vegetables`,
                supplier: `Ifugao Rural Community`
            },
            {
                name: `Fruits 2`,
                price: 45.00,
                url: imageURL,
                seller: `Abra Morning Vegetables`,
                supplier: `Ifugao Rural Community`
            },
            {
                name: `Fish 3`,
                price: 55.00,
                url: imageURL,
                seller: `Baguio Fresh Vegetables`,
                supplier: `Ifugao Rural Community`
            },
            {
                name: `Meat 4`,
                price: 65.00,
                url: imageURL,
                seller: `Baguio Fresh Vegetables`,
                supplier: `Ifugao Rural Community`
            },
            {
                name: `Vegetable 5`,
                price: 25.00,
                url: imageURL,
                seller: `Baguio Fresh Vegetables`,
                supplier: `Ifugao Rural Community`
            },
        ]
    }
}