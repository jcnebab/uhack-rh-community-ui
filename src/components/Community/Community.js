import React from 'react'
import Navigation from '../common/Navigation'
import CommunityBoard from './CommunityBoard'
import Footer from '../common/Footer'

const Community = props => {
    return (
        <>
            <Navigation />
            <CommunityBoard {...props} />
            <Footer />
        </>
    )
}

export default Community
