import React, { useState, useEffect } from 'react'
import Navigation from '../../common/Navigation'
import Footer from '../../common/Footer'
import Card from '../../common/Card'

import { store } from '../../util/store'

const CommunityProfile = ({ match }) => {
    const [profile, setProfile] = useState([])
    const [defaultAvatar, setDefaultAvatar] = useState("")

    useEffect(() => {
        function getProfile() {
            const id = match.params.communityId
            const url = `https://bibssi6nkb.execute-api.us-east-1.amazonaws.com/dev/api/supplier/${id}`
            try {
                fetch(url, {
                    cors: 'no-cors'
                })
                .then(resolvedValue => resolvedValue.json())
                .then(response => {
                    console.log(response.data[0])
                    setProfile(response.data[0])
                })
            } catch(error) {
                console.error(error)
            }
        }
        getProfile()
    }, [match, profile.name, profile.profile_pic])

    useEffect(() => {
        function getAvatar() {
            try {
                fetch(`https://ui-avatars.com/api/?name=${profile.name}`, {
                cors: 'no-cors'
                })
                .then(responseValue => responseValue)
                .then(response => {
                    setDefaultAvatar(response.url)
                })
            } catch (error) {
                console.error(error)
            }
        }
        if(profile) {
            getAvatar()
        }
    }, [profile])

    function handleAddToCart() {
        //add specific product to cart
    }

    return (
        <>
            <Navigation />
            <div className="CommunityProfile">
                {(profile.profile_pic || defaultAvatar) && <img 
                    src={profile.profile_pic == null ? defaultAvatar : profile.profile_pic} 
                    alt={`${profile.name}`}
                    className="ProfilePic"
                />}
                <p>Name: {profile.name}</p>
                <p>Community Story: {profile.story}</p>
                <p>Address: {profile.address}</p>
                <p>Contact Information: {profile.mobile_number}</p>

                <div className="CommunityProducts">
                    <h2>Products offered:</h2>
                    <div class="CommunityProductList">
                        {
                            store.best_selling.products.slice(0, 3).map((product, i) => (
                                <Card 
                                    key={`${product.name}-${i}`} 
                                    data={product} 
                                    forSale 
                                    buttonName="Add To Cart"
                                />
                            ))
                        }
                    </div>
                </div>
            </div>
            <Footer />
        </>
    )
}

export default CommunityProfile
