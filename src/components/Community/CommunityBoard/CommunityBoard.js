import React from 'react'
import Button from '../../common/Button'
import SearchInput from '../../common/SearchInput/SearchInput';

const CommunityBoard = ({ history }) => {
    return (
        <div className="CommunityBoard">
            <Button 
                name="View Available Communities"
                className="ButtonInput ViewButton"
                setFunction={() => history.push("/communities/grid")}
            />
            <SearchInput placeholder="Search a community"/>
        </div>
    )
}

export default CommunityBoard
