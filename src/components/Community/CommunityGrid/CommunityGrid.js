import React, { useState, useEffect } from 'react'
import { NavLink } from 'react-router-dom'
import Navigation from '../../common/Navigation'
import Footer from '../../common/Footer'

const CommunityGrid = props => {
    const [ communityList, setCommunityList ] = useState([])

    useEffect(() => {
        async function getCommunityList() {
            const url = `https://bibssi6nkb.execute-api.us-east-1.amazonaws.com/dev/api/suppliers?per_page=10&page=1`
            try {
                fetch(url, {
                    cors: 'no-cors'
                })
                .then(resolvedValue => resolvedValue.json())
                .then(response => {
                    setCommunityList(response.data)
                })
            } catch (error) {
                console.error(error)
            }
        }
        getCommunityList()
    }, [])

    const imageURL = `https://storage.googleapis.com/web-media-upload/category_placeholder-paper-paper-products.png`

    return (
        <>
            <Navigation />
            <div className="CommunityGrid">
                {
                   communityList ? communityList.map((community, id) => (
                        <div key={id} className="community">
                            <img src={imageURL} alt="community"/>
                            <div className="hidden">
                                <p>{community.name}</p>
                                <NavLink to={`/communities/profile/${community.id}`}>View Details</NavLink>
                            </div>
                        </div>
                    )) : <p>Pulling community data... Please wait.</p>
                }
            </div>
            <Footer />
        </>
    )
}

export default CommunityGrid
