import React, { useState } from 'react'
import Navigation from '../common/Navigation'
import LandingPageOwners from '../LandingPageOwners'
import LandingPageConsumers from '../LandingPageConsumer'
import Footer from '../common/Footer'

const Home = () => {
	return (
		<>
			<Navigation />
			{
				isConsumer ? <LandingPageConsumers />
					: <LandingPageOwners />
			}
			<Footer />
		</>
	)
}

export default Home
