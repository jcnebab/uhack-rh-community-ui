import React from 'react'

const Navigation = () => {
    return (
        <nav className="Navigation">
            <h2>VARIEGoods | The Varieties of Merchanting Goods of Rural Communities to SME's Nationwide</h2>
        </nav>
    )
}

export default Navigation
