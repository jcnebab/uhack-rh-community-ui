import React from 'react'

const Button = ({ className, name, setFunction }) => {
	return (
		<button 
			className={className}
			onClick={setFunction}
		>
		{name}
		</button>
	)
}

export default Button