import React from 'react'

const Card = ({ data, forSale, buttonName }) => {
    return (
        <div className="Card">
            {data.url && <img src={data.url} alt={data.name} />}
            <p>Name: {data.name}</p>
            <p>Price: {data.price}</p>
            <p>Seller: {data.seller}</p>
            <p>Supplier: {data.supplier}</p>
            {forSale && <button className="CardButton" >{buttonName}</button>}
        </div>
    )
}

export default Card