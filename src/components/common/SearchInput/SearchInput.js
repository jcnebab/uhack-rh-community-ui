import React from 'react'

const SearchInput = ({ name, placeholder }) => {
    return (
        <div className="SearchInput">
            <input type="text" name={name} placeholder={placeholder} />
        </div>
    )
}

export default SearchInput
