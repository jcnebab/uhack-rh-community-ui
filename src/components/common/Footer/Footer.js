import React from 'react'
import { NavLink } from 'react-router-dom'
import { FacebookIcon } from '../Icons'

const Footer = props => {
    return (
        <footer className="Footer">
            <nav>
                <div className="MultipleChildLinks">
                    <div className="brand"><NavLink to="/" >VARIEGoods</NavLink></div>
                    <div className="WithChildLinks">
                        <div className="Privacy"><NavLink to="/privacy" >Privacy</NavLink></div>
                        <div><NavLink to="#" >Terms</NavLink></div>
                    </div>
                </div>
                <div>
                    <div><NavLink to="#" ><FacebookIcon fill="#133840"/></NavLink></div>
                </div>
                <div className="WithChildLinks">
                    <div className="HowItWorks"><NavLink to="/how-it-works" >How It Works?</NavLink></div>
                    <div><NavLink to="/commitment" >Our Commitment</NavLink></div>
                </div>
            </nav>
        </footer>
    )
}

export default Footer
