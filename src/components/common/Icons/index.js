import React from 'react'

export const FacebookIcon = ({ fill }) => {
    return (
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="24"
          height="24"
          data-name="Layer 45"
          viewBox="0 0 24 24"
        >
          <path
            fill={fill}
            d="M16.75 9H13.5V7a1 1 0 011-1h2V3H14a4 4 0 00-4 4v2H8v3h2v9h3.5v-9H16z"
          />
        </svg>
    )
}
