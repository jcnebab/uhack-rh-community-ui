import React, { useState } from 'react'
import Button from '../common/Button'
import Navigation from '../common/Navigation';
import Footer from '../common/Footer';

const MainLandingPage = ({ history }) => {
  // const [isConsumer, setIsConsumer] = useState(false)

    return (
        <>
            <Navigation />
            <div className="MainLanding">
                <div className="CallToActionButtons">
                    <Button 
                        className="ButtonInput"
                        name="Get your food here."
                        setFunction={() => history.push("/home/consumer")}
                    />
                    <span>or</span>
                    <Button 
                        className="ButtonInput"
                        name="Boost your food business here." 
                        setFunction={() => history.push("/home/owners")}
                    />
                </div>
                <div className="TagLine" >
                    <h3>Supporting Agriculture. Boosting SME's. A Variety of Goodness. </h3>
                </div>
            </div>
            <Footer />
        </>
    )
}

export default MainLandingPage
