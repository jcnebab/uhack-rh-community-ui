import React, { useEffect } from 'react'
import { BrowserRouter, Route } from 'react-router-dom'
import { geolocated } from "react-geolocated"

//import Home from './components/Home'
import Community from './components/Community'
import CommunityGrid from './components/Community/CommunityGrid'
import CommunityProfile from './components/Community/CommunityProfile'
import MainLandingPage from './components/MainLandingPage/MainLandingPage'
import Products from './components/Products'
import LandingPageConsumer from './components/LandingPageConsumer'
import LandingPageOwners from './components/LandingPageOwners'

import './App.css'

const App = ({ match, history }) => {

	useEffect(() => {
		window.document.title = "VARIEGoods | The Varieties of Merchanting Goods of Rural Communities to SME's Nationwide"
	}, [])
	
	return (
		<BrowserRouter>
			<main className="App">
				<Route 
					path="/" 
					exact
					component={props => <MainLandingPage {...props}/>} 
				/>
				<Route 
					path="/home/consumer" 
					component={props => <LandingPageConsumer {...props}/>} 
				/>
				<Route 
					path="/home/owners" 
					component={props => <LandingPageOwners {...props}/>} 
				/>
				<Route 
					path="/products" 
					component={props => <Products {...props}/>} 
				/>
				<Route
					path="/owners/communities"
					component={props => <Community {...props} />}
				/>
				<Route
					path="/communities/grid"
					component={props => <CommunityGrid {...props}/>}
				/>
				<Route
					path={`/communities/profile/:communityId`}
					component={props => <CommunityProfile {...props} />}
				/>
			</main>
		</BrowserRouter>
	)
}

export default geolocated({
    positionOptions: {
        enableHighAccuracy: true,
        maximumAge: 0,
        timeout: Infinity,
    },
    watchPosition: false,
    userDecisionTimeout: null,
    suppressLocationOnMount: true,
    geolocationProvider: navigator.geolocation,
    isOptimisticGeolocationEnabled: true
})(App)
